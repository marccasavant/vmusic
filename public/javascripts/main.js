(function(){
	var app = angular.module('myApp', [])

		.config(function($interpolateProvider){
	        $interpolateProvider.startSymbol('<%').endSymbol('%>');
	    }
	);
	app.controller('StoreController', [ '$http',function($http){
		
		var store = this;

		store.people = [];

		$http.get('/users')

			.success(function(data){
				store.people = data;
			});

	}]);

}());

