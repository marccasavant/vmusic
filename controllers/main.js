var Main = function(){
	console.log("main loaded");
};

Main.prototype = {
	index: function(req, res){
		res.render('main', {main: 'This is the main variable'});
	}
};

module.exports = new Main();