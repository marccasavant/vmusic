function Users(){
	console.log('included');
};

Users.prototype = {
	index: function(req, res){
		res.send([
			{
				"first_name" : "Marc",
				"last_name" : "Casavant",
				"age" : 25,
				"position" : "Web Developer"
			},
			{
				"first_name" : "Angie",
				"last_name" : "Casavant",
				"age" : 63,
				"position" : "Mom"	
			},
			{
				"first_name" : "Christa",
				"last_name" : "Casavant",
				"age" : 25,
				"position" : "Sister"
			},
			{
				"first_name" : "Heath",
				"last_name" : "Emmerson",
				"age" : 29,
				"position" : "Friend"
			}]
		);
	},
	create: function(req, res){
		console.log('created data');
	},
	update: function(req, res){
		console.log('updated data');
	},
	delete: function(req, res){
		console.log('delete data');
	},
	save: function(req, res){
		console.log('saved data');
	}
};

module.exports = new Users();