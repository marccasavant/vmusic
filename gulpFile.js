var config  = require('./package'),
	gulp    = require('gulp'),
	util    = require('gulp-util'),
	concat  = require('gulp-concat'),
	compass = require('gulp-compass'),
	uglify  = require('gulp-uglify'),
	minCSS  = require('gulp-minify-css'),
	rename  = require('gulp-rename'),
	colors  = require('colors'),
	replace = require('gulp-replace');

var paths = {
	styles: {
		app: 'public/src/sass/**/*.sass',
		vendor: [
			'bower_components/foundation/scss/foundation/_functions.scss',
			'bower_components/foundation/scss/foundation/components/_global.scss',
			'bower_components/foundation/scss/foundation/components/_grid.scss',
			'bower_components/foundation/scss/foundation/components/_type.scss'
		],
		src: 'public/src/sass/vendor/',
		dest: 'public/stylesheets'
	}
};

gulp.task('sass', function(){
	gulp.src(paths.styles.vendor)
		// Fix relative path in functions.scss at top of file
		// then move the files to vendor folder for compass to import when compiling foundation
		.pipe(replace(/..\/functions/g, 'functions'))
		.pipe(gulp.dest(paths.styles.src));

	gulp.src(paths.styles.app)
		// Substitute information for config.rb
		.pipe(compass({
			css: './public/stylesheets',
			sass: './public/src/sass',
			image: './public/images'
		}))
		.pipe(minCSS())
		.pipe(gulp.dest(paths.styles.dest));
});

// Watch the following directories

gulp.task('watch', function(){
	gulp.watch(paths.styles.vendor, ['sass']);
	gulp.watch(paths.styles.app, ['sass']);
});

gulp.task('default', ['sass', 'watch']);
